/**
 * SafeTrip React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './src/Screens/Authentication';
import SignUp from './src/Screens/SignUp';
import EmailVerification from './src/Screens/SignUp/EmailVerification';
import EmailCodeVerification from './src/Screens/SignUp/EmailCodeVerification';
import PersonalInformation from './src/Screens/Personal_Information';
import PhoneVerification from './src/Screens/Personal_Information/PhoneVerification';
import PhoneCodeVerification from './src/Screens/Personal_Information/PhoneCodeVerification';
import ThemePreference from './src/Screens/ThemePreference';
import Splash from './src/Screens/Splash';
import Start from './src/Screens/Splash/Start';
import Home from './src/Screens/Home';

const Stack = createStackNavigator();

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" hidden translucent={true} />
      <NavigationContainer>
        <Stack.Navigator name="" initialRouteName={Start}
          options={() => ({
            headerShown: false,
            StatusBar: ''
          })} >
          <Stack.Screen name="Start" component={Start}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="Splash" component={Splash}
            options={() => ({
              headerShown: false
            })} />

          <Stack.Screen name="Home" component={Home}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="Login" component={Login}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="SignUp" component={SignUp}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="EmailVerification" component={EmailVerification}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="EmailCodeVerification" component={EmailCodeVerification}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="PersonalInformation" component={PersonalInformation}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="PhoneVerification" component={PhoneVerification}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="PhoneCodeVerification" component={PhoneCodeVerification}
            options={() => ({
              headerShown: false
            })} />
          <Stack.Screen name="ThemePreference" component={ThemePreference}
            options={() => ({
              headerShown: false
            })} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
