//import liraries
import React, { Component } from 'react';
import { View, Text, TextInput, ScrollView, Dimensions, SafeAreaView, StyleSheet } from 'react-native';
import Icon1 from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
//import Scan_icon from 'react-native-vector-icons/AntDesign';  scan1 name icon
import Scan_icon from 'react-native-vector-icons/MaterialCommunityIcons'; //credit-card-scan
import { TouchableOpacity } from 'react-native-gesture-handler';
import SlidingUpPanel from 'rn-sliding-up-panel';
const { height } = Dimensions.get('window')


// create a component
class Profile extends Component {
    constructor() {
        super();
        this.state = { date: "2020-10-28" },
            edit_mode = false


    }

    //enable edit mode and icons on the scrollview and footer is linked with the below method
    componentHideAndShow = () => {
        this.setState(previousState => ({ edit_mode: !previousState.edit_mode }))
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ flex: 1 }}>
                    {/* Start of Header */}
                    <View style={{ flex: .2 }}>
                        <View style={styles.header}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}><Icon name="arrow-left" color={'red'} size={25} /></TouchableOpacity>
                            <Text style={{ alignItems: 'center', width: '80%', textAlign: 'center', fontSize: 20 }}>Personal Information</Text>
                            <TouchableOpacity onPress={() => this.componentHideAndShow()}><Text style={{ width: '100%', textAlign: 'center', fontSize: 20, color: 'red' }}>{this.state.edit_mode ? "EDIT " : "DONE"} </Text></TouchableOpacity>
                        </View>
                    </View>
                    {/* EOF header body starts here */}
                    {/* profile photo circular image */}
                    <View style={{ flex: 1 }}>
                        <View style={styles.body_first_half}>
                            <View style={[styles.circle, { alignContent: "center" }]}>
                                <Text style={{ color: 'grey', textAlign: "center", padding: 20, margin: 10, fontSize: 20 }}>ADD PROFILE IMAGE</Text>
                            </View>
                        </View>
                    </View>
                    {/* EOF circular image */}
                    <View style={styles.body_sec_half}>
                        <ScrollView style={{ width: '100%' }}>
                            {/* 1st item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>First name:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="givenName" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 2nd item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Last name:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="familyName" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 3rd item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Email Address:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput textContentType='emailAddress' keyboardType='email-address' placeholder="" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 4th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Phone number:</Text>
                                </View>
                                <TouchableOpacity style={{ width: '100%', zIndex: 99 }} onPress={() => this.props.navigation.navigate('PhoneVerification')}>

                                    <View style={styles.item_view}>
                                        <TextInput onFocus={() => this.props.navigation.navigate('PhoneVerification')} placeholder="" textContentType="creditCardNumber" style={styles.input} />
                                        {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                    </View>
                                </TouchableOpacity>

                            </View>

                            {/* 5th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Date of Birth:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 6th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Gender:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput keyboardType='default' placeholder="" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 7th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Nationality:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>
                            {/* 8th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Street Address:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="fullStreetAddress" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 9th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>City:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="addressCity" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 10th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>State:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="addressState" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>
                            {/* 11th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Zipcode:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="postalCode" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 12th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>ID number:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" textContentType="creditCardNumber" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 13th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>ID expiration date:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 14th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Passport Number:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 15th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Passport expiration Date:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 16th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Driver license Number:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 17th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Driver license Expiration Date:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 18th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Birth City:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" textContentType='addressCity' style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 19th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Birth State:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" textContentType='addressState' style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>

                            {/* 20th item */}
                            <View style={{ flexDirection: "column", margin: 10, justifyContent: "center", alignItems: "flex-start" }}>
                                <View>
                                    <Text style={{ fontSize: 12, color: "grey" }}>Birth Country:</Text>
                                </View>
                                <View style={styles.item_view}>
                                    <TextInput placeholder="--" textContentType='countryName' style={styles.input} />
                                    {this.state.edit_mode ? <Icon1 name="checkmark-done-circle-sharp" color={'red'} size={25} /> : null}
                                </View>
                            </View>
                        </ScrollView>
                        {/* Footer view Will be visible based on the EDit/Done button View on the top right corner */}
                        {/* bsed on the edit or done button on top right the footer is visible/gone  */}
                        {
                            // Display the content in screen when state object "content" is true.
                            // Hide the content in screen when state object "content" is false. 
                            this.state.edit_mode ? <View style={styles.footer} >
                                <SlidingUpPanel draggableRange={{ top: height / 2.8, bottom: 60 }}
                                    animatedValue={this._draggedValue}
                                    showBackdrop={false} ref={c => this._panel = c}>
                                    <View style={styles.slide_drawer}>
                                        <View style={{ marginTop: 10, width: '18%', height: '1%', backgroundColor: '#DEE8EA', borderRadius: 5 }}></View>
                                        {/* <Text>Here is the content inside panel</Text>
               <Button title='Hide' onPress={() => this._panel.hide()} /> */}
                                        <Text style={{ fontSize: 20, }}>your documents</Text>
                                        {/* Box view */}
                                        <View style={styles.cardHolder}>
                                            <View style={styles.cardShadow}>
                                                <View style={[styles.cardContainer, { padding: 10, flexDirection: 'row', justifyContent: "flex-start" }]} >
                                                    <Text style={{ width: '80%', fontSize: 20, }}> ID Document </Text>
                                                    <Scan_icon style={{ alignItems: 'flex-end' }} name="credit-card-scan" color={'red'} size={25} />

                                                </View>
                                            </View>
                                        </View>

                                        <View style={styles.cardHolder}>
                                            <View style={styles.cardShadow}>
                                                <View style={[styles.cardContainer, { padding: 10, flexDirection: 'row', justifyContent: "flex-start" }]} >
                                                    <Text style={{ width: '80%', fontSize: 20, }}> Passport </Text>
                                                    <Scan_icon style={{ alignItems: 'flex-end' }} name="credit-card-scan" color={'red'} size={25} />

                                                </View>
                                            </View>
                                        </View>

                                        <View style={styles.cardHolder}>
                                            <View style={styles.cardShadow}>
                                                <View style={[styles.cardContainer, { padding: 10, flexDirection: 'row', justifyContent: "flex-start" }]} >
                                                    <Text style={{ width: '80%', fontSize: 20, }}> Driver License </Text>
                                                    <Scan_icon style={{ alignItems: 'flex-end' }} name="credit-card-scan" color={'red'} size={25} />

                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </SlidingUpPanel>
                            </View> : null
                        }



                    </View>
                    {/* EOF body sec half */}
                </View>
            </SafeAreaView >
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    }, subContainer: {
        justifyContent: 'flex-start', alignItems: 'center'
    }, header: {
        flexDirection: 'row', justifyContent: 'space-between', alignContent: "center", alignSelf: "baseline", alignItems: 'flex-start', width: '100%', padding: 10
    }, body: {
        justifyContent: 'flex-start', width: '100%'
    }, body_first_half: {
        marginTop: 15,
        width: 180, justifyContent: "center", alignItems: "center", alignSelf: "center", padding: 10
    }, circle: {
        borderColor: 'grey',
        borderWidth: 1, borderStyle: "dotted", marginBottom: 5,
        width: 160,
        height: 160,
        borderRadius: 160 / 2,
    }, body_sec_half: {
        flex: 1.8,
        alignItems: "flex-start", padding: 5, width: '100%'
    }, item_view: {
        width: '99%', height: 40, flexDirection: 'row', justifyContent: "flex-start", borderBottomWidth: 1, borderBottomColor: '#C0C0C0'
    },
    input: {
        fontSize: 18,
        width: '96%',
    }, footer: {
        width: '100%',

    }, slide_drawer: {
        flex: 1,
        backgroundColor: 'white',
        borderColor: '#DEE8EA',
        borderWidth: 2,
        borderRadius: 18,
        borderStartColor: '#9EBABF46',
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: '#9EBABF46',
        shadowOffset: {
            width: 100,
            height: 2,
        },
        shadowOpacity: 10,
        shadowRadius: 7.22,
        elevation: 3,
    }, cardHolder: {
        width: '100%',
        padding: 8,
        borderColor: '#DEE8EA',

    },
    cardShadow: {
        borderRadius: 5,
        backgroundColor: 'transparent',
        shadowColor: '#9EBABF46',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    cardContainer: {
        backgroundColor: '#fff',
        borderRadius: 5,

    },
})

//make this component available to the app
export default Profile;
