import React, { Component } from 'react'
import { Text, TextInput, StyleSheet, View, SafeAreaView } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class PhoneVerification extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.subContainer}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}><Icon name="arrow-left" color={'red'} size={25} /></TouchableOpacity>
                        <Text style={{ width: '80%', textAlign: 'center', fontSize: 20 }}>Phone Verification</Text>
                    </View>
                    <View style={{ padding: 10 }}>
                        <Text style={{ color: '#000', fontSize: 18 }}>Enter your phone number to receivew verification code.</Text>
                    </View>
                    <View style={{ marginVertical: 50 }}>
                        <TextInput style={{ fontSize: 30, opacity: 0.5, justifyContent: "flex-start", borderBottomWidth: 1, borderBottomColor: '#C0C0C0' }} placeholder='9985699856' ></TextInput></View>
                    <View style={{ width: '70%', position: 'relative', left: -50, marginBottom: 50 }}>
                        <Text style={{ textAlign: 'left', fontSize: 17 }}>Code is valid for 3 minutes <Text>Resend code</Text></Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PhoneCodeVerification')} style={{ paddingVertical: 15, paddingHorizontal: 100, backgroundColor: '#F52936' }}><Text style={{ color: '#FFF', fontSize: 18 }}>SEND CODE</Text></TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    }, subContainer: {
        justifyContent: 'center', alignItems: 'center', padding: 10,
    }, header: {
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', width: '100%', padding: 10
    }
})
