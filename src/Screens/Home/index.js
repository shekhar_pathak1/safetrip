import React, { Component } from 'react'
import { Text, StyleSheet, TouchableOpacity, Image, View } from 'react-native'
import Icon from 'react-native-vector-icons/EvilIcons';
import Right_arrow_icon from 'react-native-vector-icons/AntDesign'; //credit-card-scan
import Circle_check_icon from 'react-native-vector-icons/MaterialIcons';
import Circle_icon from 'react-native-vector-icons/MaterialIcons';
//
import User_icon from 'react-native-vector-icons/Feather';
import Credit_card from 'react-native-vector-icons/SimpleLineIcons';
import Tube_icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Docs_icon from 'react-native-vector-icons/Ionicons';


export default class Home extends Component {
  render() {
    return (
      <View style={styles.main_container}>
        <View style={styles.first_half}>
          <Image style={{ width: '100%' }} source={require('../../resources/images/splashscreen.png')} />
          <View style={styles.header}><Image style={{ marginTop: 5 }} source={require('../../resources/images/HYPERLOOPTT_Logo_White_Red_RGB.png')} />
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}><Icon name="gear" color={'white'} size={25} /></TouchableOpacity>
          </View>
          <View style={{ width: '100%', position: 'absolute', bottom: 0, }}>
            <View style={styles.first_half_cardHolder}>
              <View style={styles.first_half_cardShadow}>
                <View style={[styles.first_half_cardContainer, { padding: 10, flexDirection: 'row', justifyContent: "flex-start" }]} >
                  <Image style={styles.circle} source={require('../../resources/images/HYPERLOOPTT_Logo_White_Red_RGB.png')} />
                  <Text style={{ width: '65%', fontSize: 15, textAlign: 'center', fontWeight: 'bold' }}> Share my SafeTrip ID </Text>
                  <Right_arrow_icon style={styles.arrow_icon_circle} name="arrowright" color={'red'} size={30} />
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.sec_half}>
          <View style={styles.sec_half_main_view}>

            {/* 1st item */}
            <View style={styles.sec_half_cardHolder}>
              <View style={styles.sec_half_cardShadow}>
                <TouchableOpacity style={{}} onPress={() => this.props.navigation.navigate('PersonalInformation')}>
                  <View style={[styles.sec_half_cardContainer, { padding: 9, flexDirection: 'row', justifyContent: "flex-start" }]} >
                    <View style={{ flexDirection: 'column', width: '80%', marginLeft: 5 }}>
                      <Text style={{ width: '70%', fontSize: 20, textAlign: 'left', color: 'black', fontWeight: 'bold' }}> Personal </Text>
                      <View style={{ flexDirection: 'row', width: '80%', alignContent: 'center', marginLeft: 5 }}>
                        <Circle_icon style={{ alignSelf: 'center' }} name="radio-button-unchecked" color={'red'} size={10} />
                        <Text style={{ width: '70%', fontSize: 12, textAlign: 'left', color: 'red' }}> 7/8 Completed </Text>
                      </View>
                    </View>
                    <User_icon name="user" color={'grey'} size={35} />
                  </View>
                </TouchableOpacity>

              </View>
            </View>

            {/* 2nd item */}
            <View style={styles.sec_half_cardHolder}>
              <View style={styles.sec_half_cardShadow}>
                <View style={[styles.sec_half_cardContainer, { padding: 9, flexDirection: 'row', justifyContent: "flex-start" }]} >
                  <View style={{ flexDirection: 'column', width: '80%', marginLeft: 5 }}>
                    <Text style={{ width: '70%', fontSize: 20, textAlign: 'left', fontWeight: 'bold', color: 'black', }}> My Credentials </Text>
                    <View style={{ flexDirection: 'row', width: '80%', alignContent: 'center', marginLeft: 5 }}>
                      <Circle_icon style={{ alignSelf: 'center' }} name="check-circle" color={'red'} size={10} />
                      <Text style={{ width: '70%', fontSize: 12, textAlign: 'left', color: 'red' }}> 4 documents </Text>
                    </View>
                  </View>
                  <Credit_card name="credit-card" color={'grey'} size={35} />
                </View>
              </View>
            </View>

            {/* 3rd item */}
            <View style={styles.sec_half_cardHolder}>
              <View style={styles.sec_half_cardShadow}>
                <View style={[styles.sec_half_cardContainer, { padding: 9, flexDirection: 'row', justifyContent: "flex-start" }]} >
                  <View style={{ flexDirection: 'column', width: '80%', marginLeft: 5 }}>
                    <Text style={{ width: '100%', fontSize: 18, textAlign: 'left', fontWeight: 'bold' }}> COVID-19 self assessment </Text>
                    <View style={{ flexDirection: 'row', width: '80%', alignContent: 'center', marginLeft: 5 }}>
                      <Circle_icon style={{ alignSelf: 'center' }} name="radio-button-unchecked" color={'red'} size={10} />
                      <Text style={{ width: '90%', fontSize: 12, textAlign: 'left', color: 'red' }}> last report 08/24/2020</Text>
                    </View>
                  </View>
                  <Tube_icon name="test-tube" color={'grey'} size={35} />
                </View>
              </View>
            </View>

            {/* 4th item */}
            <View style={styles.sec_half_cardHolder}>
              <View style={styles.sec_half_cardShadow}>
                <View style={[styles.sec_half_cardContainer, { padding: 9, flexDirection: 'row', justifyContent: "flex-start" }]} >
                  <View style={{ flexDirection: 'column', width: '80%', marginLeft: 5 }}>
                    <Text style={{ width: '70%', fontSize: 20, textAlign: 'left', fontWeight: 'bold' }}> Contact tracing </Text>
                    <View style={{ flexDirection: 'row', width: '80%', alignContent: 'center', marginLeft: 5 }}>
                      <Circle_icon style={{ alignSelf: 'center' }} name="radio-button-unchecked" color={'red'} size={10} />
                      <Text style={{ width: '90%', fontSize: 12, textAlign: 'left', color: 'red' }}> Deactivated -- Click to Activate </Text>
                    </View>
                  </View>
                  <User_icon name="user" color={'grey'} size={35} />
                </View>
              </View>
            </View>

            {/* 5th item */}
            <View style={styles.sec_half_cardHolder}>
              <View style={styles.sec_half_cardShadow}>
                <View style={[styles.sec_half_cardContainer, { padding: 9, flexDirection: 'row', justifyContent: "flex-start" }]} >
                  <View style={{ flexDirection: 'column', width: '80%', marginLeft: 5 }}>
                    <Text style={{ width: '70%', fontSize: 20, textAlign: 'left', fontWeight: 'bold' }}> Tested Positive ? </Text>
                    <View style={{ flexDirection: 'row', width: '80%', alignContent: 'center', marginLeft: 5 }}>
                      <Circle_icon style={{ alignSelf: 'center' }} name="check-circle" color={'red'} size={10} />
                      <Text style={{ width: '80%', fontSize: 12, textAlign: 'left', color: 'red' }}> Status sharing active. 14days </Text>
                    </View>
                  </View>
                  <Docs_icon name="md-document-text-outline" color={'grey'} size={35} />
                </View>
              </View>
            </View>




          </View>

        </View>

      </View >
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#000', position: 'absolute',
    flexDirection: 'row', justifyContent: 'space-between', alignSelf: "baseline", width: '100%', padding: 10

  }, circle: {
    borderColor: 'black',
    borderWidth: .5,
    //borderStyle: "dotted",
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
  },
  first_half: {
    flex: .85,
  },
  sec_half: {
    flex: 1.15,

  },
  first_half_cardHolder: {
    width: '98%',
    margin: 5,
    borderColor: '#DEE8EA',
    opacity: 0.5,
  },
  first_half_cardShadow: {
    borderRadius: 5,
    backgroundColor: 'transparent',
    shadowColor: '#9EBABF46',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  first_half_cardContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  arrow_icon_circle: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    borderWidth: 1,
    padding: 7,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
    borderColor: 'transparent',
    borderStyle: "dotted",
  },
  sec_half_main_view: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: '#DEE8EA',
    borderWidth: 2,
    borderRadius: 18,
    borderStartColor: '#9EBABF46',
    alignItems: 'center',
    justifyContent: 'flex-start',
    shadowColor: '#9EBABF46',
    shadowOffset: {
      width: 100,
      height: 2,
    },
    shadowOpacity: 10,
    shadowRadius: 7.22,
    elevation: 3,
  },
  sec_half_cardHolder: {
    width: '98%',
    margin: 5,
    borderColor: '#DEE8EA',
    opacity: 0.5,
  },
  sec_half_cardShadow: {
    borderRadius: 5,
    backgroundColor: 'transparent',
    shadowColor: '#9EBABF46',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  sec_half_cardContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },


})
