import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, TextInput } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class EmailCodeVerification extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.subContainer}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}><Icon name="arrow-left" color={'red'} size={25} /></TouchableOpacity>
                        <Text style={{ width: '80%', textAlign: 'center', fontSize: 20 }}>Email Verification</Text>
                    </View>
                    <View style={{ padding: 10, position: 'relative', left: -50 }}>
                        <Text style={{ color: '#000', fontSize: 18 }}>Enter the code you received.</Text>
                    </View>
                    <View style={{ marginVertical: 50, marginBottom: 150 }}>
                        <TextInput placeholder="Verification Code" style={styles.input} />
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PersonalInformation')} style={{ paddingVertical: 15, paddingHorizontal: 100, backgroundColor: '#F52936' }}><Text style={{ color: '#FFF', fontSize: 18 }}>Verify Email</Text></TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    }, subContainer: {
        justifyContent: 'center', alignItems: 'center', padding: 10,
    }, header: {
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', width: '100%', padding: 10
    }, input: {
        fontSize: 25,
        width: 350,
        marginBottom: 30,
        marginTop: 20,
        borderBottomWidth: 2,
        borderBottomColor: '#C0C0C0'
    },
})
