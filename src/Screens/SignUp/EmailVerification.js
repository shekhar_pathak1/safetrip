import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class EmailVerification extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.subContainer}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}><Icon name="arrow-left" color={'red'} size={25} /></TouchableOpacity>
                        <Text style={{ width: '80%', textAlign: 'center', fontSize: 20 }}>Email Verification</Text>
                    </View>
                    <View style={{ padding: 10 }}>
                        <Text style={{ color: '#000', fontSize: 18 }}>We need to verify your email. Please confirm the email below is correct to send the code too.</Text>
                    </View>
                    <View style={{ marginVertical: 50 }}>
                        <Text style={{ fontSize: 30, opacity: 0.5 }}>mar………er@gmail.com</Text>
                    </View>
                    <View style={{ width: '70%', position: 'relative', left: -50, marginBottom: 50 }}>
                        <Text style={{ textAlign: 'left', fontSize: 17 }}>Code is valid for 3 minutes <Text>Resend code</Text></Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('EmailCodeVerification')} style={{ paddingVertical: 15, paddingHorizontal: 100, backgroundColor: '#F52936' }}><Text style={{ color: '#FFF', fontSize: 18 }}>Send Code</Text></TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    }, subContainer: {
        justifyContent: 'center', alignItems: 'center', padding: 10,
    }, header: {
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', width: '100%', padding: 10
    }
})
