import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { TextInput, SafeAreaView, CheckBox, Button, Linking } from 'react-native'

class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            check: false
        }
    }
    CheckBoxTest() {
        this.setState({
            check: !this.state.check
        })
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.subContainer}>
                    <Text style={styles.baseText}> HYPERLOOP<Text style={styles.innerText}>TT</Text> </Text>

                    <View style={styles.container}>
                        <Text style={styles.baseText1}>Welcome to SafeTrip</Text>
                        <Text style={styles.baseText}>Create your account</Text>
                    </View>
                    <View>
                        <TextInput placeholder="First name" style={styles.input} />
                        <TextInput placeholder="Last name" style={styles.input1} />
                        <TextInput placeholder="Email" autoCompleteType='email' style={styles.input2} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <CheckBox value={this.state.check} onChange={() => this.CheckBoxTest()} style={{ marginTop: 15 }} />
                        <Text stle={styles.baseText2}> {"\n"}I agree to  <Text style={{ textDecorationLine: 'underline' }} onPress={() => Linking.openURL('http://google.com')}>Terms </Text>and<Text style={{ textDecorationLine: 'underline' }} onPress={() => Linking.openURL('http://google.com')} > Privacy</Text></Text>
                    </View>
                    <View style={styles.buttonContinue}>
                        <Button title="continue" color='red'
                            onPress={() => this.props.navigation.navigate('EmailVerification')}
                            titleStyle={{
                                fontSize: 50,
                            }}
                        />
                    </View>
                    <View style={styles.SignIn}>
                        <Text>Already have an account? <Text style={styles.In}
                            onPress={() => Linking.openURL('http://google.com')}>Sign in</Text></Text>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        color: '#000000',
    },
    subContainer: {
        justifyContent: 'center', alignItems: 'center'
    },
    container1: {
        flex: 1,
        color: '#000000',


    },
    baseText: {
        // fontFamily: "Kolkata",
        fontWeight: 'bold',
        fontSize: 20

    },
    baseText2: {
        // fontFamily: "Kolkata",
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 10,

    },
    innerText: {
        color: '#f44336',
        fontSize: 20,
        fontWeight: 'bold'
    },
    baseText1: {
        fontWeight: 'bold',
        fontSize: 30,

    },
    input: {
        width: 350,
        marginBottom: 30,
        marginTop: 20,
        borderBottomWidth: 2,
        borderBottomColor: '#C0C0C0'
    },
    input1: {
        width: 350,
        borderBottomWidth: 2,
        borderBottomColor: '#C0C0C0',
        marginBottom: 20
    },
    input2: {
        width: 350,
        borderBottomWidth: 2,
        borderBottomColor: '#C0C0C0',
        marginBottom: 20,
        marginTop: 15
    },
    buttonContinue: {

        marginTop: 180,
        width: '80%',
        marginLeft: 18,
    },
    SignIn: {
        marginLeft: 18,
        margin: 20
    },
    In:
    {
        color: 'blue',

    }
})

export default SignUp;
