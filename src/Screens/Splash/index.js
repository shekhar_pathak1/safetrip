import React, { Component } from 'react';
import { View, Text, StatusBar, TextInput, ScrollView, Dimensions, SafeAreaView, ImageBackground, StyleSheet } from 'react-native';

export class Splash extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ImageBackground source={require('../../resources/images/splashscreen.png')} style={{ width: '100%', height: '100%' }}>
                    <StatusBar hidden={true} />
                    <Text>Inside</Text>
                </ImageBackground>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

export default Splash
