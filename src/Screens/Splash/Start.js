//import liraries
import React, { Component } from 'react';
import { View, Text, StatusBar, TextInput, TouchableOpacity, ScrollView, Dimensions, Image, SafeAreaView, StyleSheet } from 'react-native';

// create a component
class Start extends Component {

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar networkActivityIndicatorVisible={true} />
                <Image source={require('../../resources/images/splashscreen.png')} style={styles.backgroundImage} style={{ width: '100%', height: '100%' }} />
                <View style={styles.main_view}>

                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={{ paddingVertical: 10, paddingHorizontal: 95, backgroundColor: '#F52936' }}><Text style={{ color: '#FFF', fontSize: 15 }}>SIGN UP</Text></TouchableOpacity>
                    </View>
                    <View style={{ justifyContent: 'center', flexDirection: 'row', marginBottom: '10%' }} >
                        <Text style={{ color: '#FFF', fontSize: 12 }}  >Already have an account? </Text><Text style={{ textDecorationLine: 'underline', color: '#FFF', fontSize: 12 }}> Sign in</Text>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: "cover", // or 'stretch'

    },
    main_view: {
        position: 'absolute',
        justifyContent: "space-evenly",
        bottom: 0,
    },
});

//make this component available to the app
export default Start;
