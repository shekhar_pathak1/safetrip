
import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, ImageBackground, ScrollView, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Orientation from 'react-native-orientation-locker';

export default class Dashboard extends Component {
    _onOrientationDidChange = (orientation) => {
        if (orientation == 'LANDSCAPE-LEFT') {
            //do something with landscape left layout
        } else {
            //do something with portrait layout
        }
    };
    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener("didFocus", () => {
            Orientation.lockToPortrait();
        });

    }
    componentWillUnmount() {
        this.focusListener.remove();
        Orientation.removeOrientationListener(this._onOrientationDidChange);
    }
    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null
        }
    }
    scanDoc(scanObj) {
        this.props.navigation.navigate('Scan', {
            scanName: scanObj
        }, { transition: 'fade' })

    }
    render() {
        return (
            <ScrollView>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={styles.box1}>
                    <ImageBackground source={require('../Resources/hyperloop.jpg')} style={styles.image}>
                        <View style={{ height: 50, width: '100%', marginTop: 60, margin: 20, flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ width: '80%', fontSize: 20, fontWeight: 'bold' }}>HYPERLOOPTT</Text>
                            <View style={styles.buttonCircle}>
                                <Icon
                                    name="md-arrow-round-forward"
                                    color="rgba(255, 255, 255, .9)"
                                    size={24}
                                />
                            </View>
                        </View>
                        <Text style={{ margin: 20, width: '100%', fontSize: 32, fontWeight: 'bold', color: 'white' }}>Welcome to Safetrip {"\n"} Let's get you setup!</Text>


                    </ImageBackground>
                </View>
                <View style={styles.box2}>

                    <Text style={{ marginTop: 30, margin: 20, marginBottom: 10, fontSize: 20 }}>To start, scan your personal identification documents by clickingone of the buttons below</Text>
                    <Text style={{ margin: 20, marginTop: 0, fontSize: 18, color: 'red' }}> You will be able to upload more documents later</Text>
                    <View style={{ height: '95%', margin: 20, justifyContent: 'space-evenly' }}>
                        <View style={{
                            height: 60, flexDirection: 'row', backgroundColor: 'white', alignItems: 'center'
                        }}>
                            <Text style={{ width: '85%', paddingLeft: 20, fontSize: 20, fontWeight: 'bold' }}>ID Card</Text>
                            <TouchableOpacity onPress={() => { this.scanDoc('IDCard') }}>
                                <View style={styles.buttonCircle}>
                                    <Icon
                                        name="md-arrow-round-forward"
                                        color="rgba(255, 255, 255, .9)"
                                        size={24}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            height: 60, flexDirection: 'row', backgroundColor: 'white', alignItems: 'center'

                        }}>
                            <Text style={{ width: '85%', paddingLeft: 20, fontSize: 20, fontWeight: 'bold' }}>Passport</Text>
                            <TouchableOpacity onPress={() => { this.scanDoc('Passport') }}>
                                <View style={styles.buttonCircle}>
                                    <Icon
                                        name="md-arrow-round-forward"
                                        color="rgba(255, 255, 255, .9)"
                                        size={24}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            height: 60, flexDirection: 'row', backgroundColor: 'white', alignItems: 'center'
                        }}>
                            <Text style={{ width: '85%', paddingLeft: 20, fontSize: 20, fontWeight: 'bold' }}>Driver Licence</Text>
                            <TouchableOpacity onPress={() => { this.scanDoc('Driver Licence') }}>
                                <View style={styles.buttonCircle}>
                                    <Icon
                                        name="md-arrow-round-forward"
                                        color="rgba(255, 255, 255, .9)"
                                        size={24}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

            </ScrollView>
        );
    }
}
//to start, scan your personal identification documents by clickingone of the buttons below
// this.props.navigation.navigate('AddName', { transition: 'fade' })
const styles = StyleSheet.create({
    box1: {
        height: '70%',
        width: '100%',
    },
    text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
    },
    image: {
        flex: 1,
        resizeMode: "cover",

    },
    box2: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        top: '60%',
        borderRadius: 40,
        backgroundColor: 'rgba(249, 247, 246, 1)'
        //249, 247, 246 

    },
    buttonCircle: {
        width: 44,
        height: 44,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 22,
        justifyContent: 'center',
        alignItems: 'center',
    },
});