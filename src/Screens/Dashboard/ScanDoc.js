//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Button } from 'react-native';
import Orientation from 'react-native-orientation-locker';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Ionicons';


// create a component
class ScanDoc extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showImage: false,
            showCamera: true,
            imagePath: ''

        }
    }
    _onOrientationDidChange = (orientation) => {
        if (orientation == 'LANDSCAPE-LEFT') {
            //do something with landscape left layout
        } else {
            //do something with portrait layout
        }
    };
    componentWillUnmount() {
        Orientation.removeOrientationListener(this._onOrientationDidChange);
    }
    componentWillMount() {
        //The getOrientation method is async. It happens sometimes that
        //you need the orientation at the moment the js starts running on device.
        //getInitialOrientation returns directly because its a constant set at the
        //beginning of the js code.
        var initial = Orientation.getInitialOrientation();
        if (initial === 'PORTRAIT') {
            //do stuff
        } else {
            //do other stuff
        }
    }
    componentDidMount() {
        const { navigation } = this.props;
        const screenName = navigation.getParam('scanName', 'NO-User');
        this.props.navigation.setParams({ title: screenName })

        Orientation.getAutoRotateState((rotationLock) => this.setState({ rotationLock }));
        //this allows to check if the system autolock is enabled or not.

        //Orientation.lockToPortrait(); //this will lock the view to Portrait
        Orientation.lockToLandscapeLeft(); //this will lock the view to Landscape
        //Orientation.unlockAllOrientations(); //this will unlock the view to all Orientations

        //get current UI orientation
        /*
        Orientation.getOrientation((orientation)=> {
          console.log("Current UI Orientation: ", orientation);
        });
    
        //get current device orientation
        Orientation.getDeviceOrientation((deviceOrientation)=> {
          console.log("Current Device Orientation: ", deviceOrientation);
        });
        */

        Orientation.addOrientationListener(this._onOrientationDidChange);
    }
    takePicture = async () => {

        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);

            this.setState({
                showCamera: false,
                showImage: true,
                imagePath: data.uri
            })
            Orientation.lockToPortrait();
            console.log(data.uri);
        }
    };
    btnAction(obj) {
        if (obj === 'good') {
            this.setState({
                showCamera: false,
                showImage: true,
            })
            Orientation.lockToPortrait();
            this.props.navigation.goBack()
        }
        else {
            this.setState({
                showCamera: true,
                showImage: false,
                imagePath: ''
            })
            Orientation.lockToLandscape();
        }

    }
    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.showImage && (
                        <View>
                            <Image style={{ margin: 10, height: '60%', width: '94%' }} source={{ uri: this.state.imagePath }} />
                            <Text style={{ fontSize: 14, margin: 10, width: '94%', alignContent: 'center', justifyContent: 'center' }}> Take a look and see if the photo is clear and legible </Text>
                            <View style={styles.buttonSection}>
                                <TouchableOpacity onPress={() => this.btnAction('good')}
                                    style={styles.looksGoodBtn}

                                >
                                    <Text style={{ color: 'white', fontSize: 20, padding: 5, textAlign: 'center', }}>LOOKS GOOD</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.btnAction('retake')}
                                    style={styles.reTakeBtn}

                                >
                                    <Text style={{ color: 'grey', fontSize: 20, padding: 5, textAlign: 'center', justifyContent: 'center' }}>RETAKE PHOTO</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    )
                }
                {
                    this.state.showCamera && (
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <RNCamera
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={RNCamera.Constants.Type.back}
                                flashMode={RNCamera.Constants.FlashMode.on}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                                    console.log(barcodes);
                                }}
                            />
                            <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                                <View style={styles.buttonCircle}>
                                    <Icon
                                        name="md-arrow-round-forward"
                                        color="rgba(255, 255, 255, .9)"
                                        size={24}
                                    />
                                    <Text style={{ fontSize: 14 }}> SNAP </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    )
                }



            </View >
        );
    }

}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    preview: {
        flex: 0.9,
        marginLeft: 60,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: '100%'
    },
    capture: {
        fontSize: 30,
        color: 'red',
        alignSelf: 'center',
        height: 60,
        padding: 20
    },
    buttonSection: {
        width: '100%',
        height: '30%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    looksGoodBtn: {
        backgroundColor: 'red',
        color: 'red',
        width: '60%',
        margin: 10,
        height: 40
    },
    reTakeBtn: {
        color: 'grey',
        width: '60%',
        height: 40,
        alignContent: 'center',
        alignItems: 'center'
    }


});

//make this component available to the app
export default ScanDoc;
