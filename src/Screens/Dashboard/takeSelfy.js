//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

// create a component
const takeSelfy = () => {
    return (
        <View style={styles.container}>
            <View style={styles.box1}>

            </View>
            <View style={styles.box2}>

            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    box1: {
        height: '80%',
        width: '100%',
    },
    box2: {
        height: '90%',
        width: '100%',
        position: 'absolute',
        top: '60%',
        borderRadius: 40,
        backgroundColor: 'rgba(249, 247, 246, 1)'
        //249, 247, 246 

    }
});

//make this component available to the app
export default takeSelfy;
